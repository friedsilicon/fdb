#define _POSIX_C_SOURCE 200809L

#include "fdb_private.h"
#include "fdb_hexdump.h"
#include "fdb_log.h"

#include <assert.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#define SKIPLINKS       10

// TODO: multi-threading support
static bool         is_initialized = false;
static fdb_ctrl_t   controller;

fctrl fcontrol()
{
    if (is_initialized) {
        return &controller;
    }

    return fctrl_init();
}

fctrl fctrl_init()
{
    if (!is_initialized) {
        controller.pid = getpid();
        is_initialized = true;
        //printf("pid: %lun", pid); 
    }

    return &controller;
}

void
fctrl_set_name(fctrl ctrl, const char* name)
{
    assert(ctrl);

    // TODO: truncation possible
    snprintf(ctrl->name, FDB_MAX_CTRL_NAME_LEN, "%s", name);
}

void fdb_controller_list_dbs(const fctrl ctrl);
void fdb_controller_list_txn(const fctrl ctrl);
fdb fdb_get_db_handle(const fctrl ctrl, int dbid)
{
    assert(ctrl);
    assert(dbid >= FDB_DB_ID_MIN);
    assert(dbid <= FDB_DB_ID_MAX);

    return ctrl->dbs[dbid];
}
ftx fdb_get_txn_handle(const fctrl ctrl, int txid);

void fctrl_add_db(fctrl ctrl, fdb db)
{
    assert(ctrl);
    assert(db);
    assert(!ctrl->dbs[db->id]);

    ctrl->dbs[db->id] = db;
}

void fctrl_remove_db(fctrl ctrl, fdb db)
{
    assert(ctrl);
    assert(db);
    assert(ctrl->dbs[db->id]);

    ctrl->dbs[db->id] = NULL;
}

void fctrl_deinit(fctrl ctrl)
{
    if (!ctrl || !is_initialized) {
        is_initialized = false;
        return;
    }

    memset(ctrl->name, 0, sizeof(ctrl->name));
    ctrl->pid = 0;
    memset(ctrl->dbs, 0, sizeof(ctrl->dbs));

    is_initialized = false;
}


