/*
 * Copyright © 2016 Shivanand Velmurugan. All Rights Reserved.
 */
#include "fdb_private.h"
#include "CppUTest/TestHarness.h"
#include "fdb/fdb.h"
#include <stdio.h>
#include <string.h>

// clang-format off
TEST_GROUP(FctrlInitGroup){};
// clang-format on

/*
TEST(FctrlInitGroup, AlreadyDeInitialized)
{
    fctrl c = NULL;

    c = fctrl_init();
    fctrl_deinit(c);
}
*/

TEST(FctrlInitGroup, InitPass)
{
    fctrl fc = NULL;

    fc = fctrl_init();
    CHECK(fc != NULL);
    //CHECK(fc->pid == getpid());
    fctrl_deinit(fc);
    fc = NULL;

    fc = fcontrol();
    CHECK(fc != NULL);
    fctrl_deinit(fc);
    fc = NULL;
    
    fctrl_deinit(fc);
}

/*
TEST(FctrlInitGroup, InitFailWithNull)
{
    fdb db = NULL;

    db = fdb_init(NULL);
    CHECK(db == NULL);
}

TEST(FctrlInitGroup, FctrlInitWithDuplicateName)
{
    fdb db = NULL;
    fdb db1 = NULL;

    db = fdb_init("foo");
    CHECK(db != NULL);
    CHECK(db->id != 0);
    STRCMP_EQUAL("foo", db->name);

    db1 = fdb_init("foo");
    CHECK(db1 != db);
    CHECK(db->id != db1->id);
    STRCMP_EQUAL("foo", db1->name);

    fdb_deinit(db);
    fdb_deinit(db1);
}
*/
